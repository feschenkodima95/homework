import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
// import Button from "./components/button/Button";
// import Modal from "./components/modal/Modal";

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);
