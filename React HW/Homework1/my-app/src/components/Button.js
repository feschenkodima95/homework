import React from "react";
import "../style/app.scss";
import Modal from "./Modal";

class Button extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="buttons__wrapper">
        <button onClick={this.props.onBtn} style={{ ...this.props.style }}>
          {this.props.text}
        </button>
      </div>
    );
  }
}
export default Button;
