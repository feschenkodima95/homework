import React from "react";
// import { useState } from "react";
import "../style/app.scss";

class Modal extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div
        className={`wrapper ${this.props.isOpened ? "open" : "close"}`}
        style={{ ...this.props.style }}
      >
        <div className="body">
          <div className="header">
            <h2>{this.props.header}</h2>
            <div
              className="close-modal"
              style={{ ...this.props.style }}
              onClick={this.props.onModalClose}
            >
              ×
            </div>
          </div>

          <div className="message">{this.props.text}</div>

          <div className="footer">
            <button className="actions" onClick={this.props.onModalClose}>
              {this.props.actions1}
            </button>
            <button className="actions" onClick={this.props.onModalClose}>
              {this.props.actions2}
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default Modal;
