import React from "react";
import Button from "./components/Button";
import Modal from "./components/Modal";
// import { useState } from "react";
// import "./App.css";
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isFirstModalActive: false,
      isSecondModalActive: false,
    };
  }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <Button
            style={{ backgroundColor: "red" }}
            onBtn={() => this.setState({ isFirstModalActive: true })}
            text={"Modal 1"}
          ></Button>

          <Button
            onBtn={() => this.setState({ isSecondModalActive: true })}
            text={"Modal 2"}
            style={{ backgroundColor: "green" }}
          ></Button>
        </header>

        <Modal
          header={"Do you want to close the window?"}
          isOpened={this.state.isFirstModalActive}
          onModalClose={() => this.setState({ isFirstModalActive: false })}
          text={"lorem ipsum dolor"}
          actions1={"Ok"}
          actions2={"Cancel"}
        ></Modal>

        <Modal
          header={"Your task has been added!"}
          isOpened={this.state.isSecondModalActive}
          onModalClose={() => this.setState({ isSecondModalActive: false })}
          text={"This my second modal window"}
          actions1={"You`re good"}
          actions2={"its dont matter"}
        ></Modal>
      </div>
    );
  }
}

export default App;
