const ipBtn = document.getElementById("ip");

async function getIp() {
  const url = "https://api.ipify.org/?format=json";
  const ipApiUrl = "http://ip-api.com/json";

  const resp = await fetch(url);
  const data = await resp.json();
  const ip = await data.ip;
  const response = await fetch(
    `${ipApiUrl}/${ip}?fields=status,message,continent,country,region,regionName,city,query`
  );
  const data2 = await response.json();
  
  let html = `<div>
    <ul>
    <li>Континент: ${data2.continent}</li>
    <li>Страна: ${data2.country}</li>
    <li>Регион: ${data2.region}</li>
    <li>Город: ${data2.city}</li>
    <li>Район: ${data2.regionName}</li>
    <li>IP: ${data2.query}</li>
    </ul>
  </div>`
  return document.body.insertAdjacentHTML('beforeend', html);
}

ipBtn.addEventListener("click", (event) => {
  event.preventDefault();
  getIp();
});
