//ajax - возможность асинхронной работы кода и возможность прислать запрос на сервер без перезагрузки страницы

function getList() {
  fetch("https://ajax.test-danit.com/api/swapi/films")
    .then((resp) => resp.json())
    .then((data) => {
      const filmsPromises = data.forEach(
        ({ episodeId, name, openingCrawl, characters }) => {
          const char = characters.map((character) => {
            return fetch(character).then((resp) => resp.json());
          });

          Promise.all(char).then((data) => {
            console.log(data);
            data.forEach((dataItem) =>
              dataItem.map((item) => {
                const charactersContainer =
                  document.getElementById("characters");
                return charactersContainer.insertAdjacentHTML(
                  "afterbegin",
                  `<li>${item.name}</li>`
                );
              })
            );
          });
          const filmsContainer = document.getElementById("films");
          filmsContainer.insertAdjacentHTML(
            "afterbegin",
            `<h3>Episode ${episodeId}. ${name}</h3>
          <p>${openingCrawl}</p>
          <ul id="characters"><ul>`
          );
        }
      );
    });
}
getList();
