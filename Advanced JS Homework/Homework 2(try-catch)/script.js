// try catch используется, когда есть риск получить ошибку, но нет возможности ее предугадать, то эта часть кода
// добавляется в try catch и это поможет отработать код до при ошибке и не обрывать отработку. Например, если
// работа кода зависит от ввода пользователя корректных данных.

const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70,
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40,
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  },
];

let container = document.getElementById("root");

let booksList = books.map((book) => {
  try {
    if (
      book.author === undefined ||
      book.name === undefined ||
      book.price === undefined
    ) {
      throw new SyntaxError("No key");
    } else {
      return `<li>Author:  ${book.author} -- ${book.name} -- ${book.price} $</li>`;
    }
  } catch (error) {
    console.error(error);
  }

 
}).join('');
container.innerHTML = `<ul>${booksList}</ul>`