// Прототипное наследование - наследование одного объекта от другого в Proto. Proto - скрытый раздел объекта со свойствами.

class Employee {
  constructor(firstName, lastName, age, salary) {
    this._firstName = firstName;
    this._lastName = lastName;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    return `${this._firstName}  ${this._lastName}`;
  }
  get age() {
    return `its ${this._age} years old.`;
  }

  get salary() {
    return `Programmers salary is ${this._salary * 3} $.`;
  }

  set name(newName) {
    if (this._name === "") {
      this._name = newName;
    } else {
      console.error("Not validation name");
    }
  }
  set salary(value) {
    if (!isNaN(this._salary)) {
      this._salary = value;
    } else {
      console.error("please, input numbers");
    }
  }

  set age(newAge) {
    if (this._age.length > 0 && !isNaN(this._age)) {
      this._age = newAge;
    } else {
      console.error("Input correct age!");
    }
  }
}

const employee = new Employee("Fabricio", "Werdum", 20, 3000);

class Programmer extends Employee {
  constructor(firstName, lastName, age, salary, lang) {
    super(firstName, lastName, age, salary);
    this._lang = lang;
  }

  get salary() {
    return `Programmers salary is ${this._salary * 4} $!`;
  }
  get lang() {
    return `This programmer knows ${this._lang} language!`;
  }
}

const programmer1 = new Programmer(
  1,
  "Feschchenko",
  25,
  1000,
  "Ukrainian"
);
const programmer2 = new Programmer("Mike", "Wazovsky", 180, 500, "Klyngonian");
const programmer3 = new Programmer("Han", "Solo", 37, 2000, "Chuweenian");

console.log(employee.salary, employee.age, employee.name);
console.log(programmer1.name = 1, programmer2.salary, programmer3.age = 18, programmer2.lang);
