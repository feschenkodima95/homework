//DOM - это структура страницы в виде древа, которое включает в себя узлы данных и меет родительские и дочерние элементы
//с этой моделью можно работать: искать элементы, добавлять, редактировать.
// let arr2;

let arrayDom1 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

let arrayDom2 = ["1", "2", "3", "sea", "user", 23];
const DOMArr = (arr, parent = document.body) => {
  let DOMListAdd = `<ul>${arr.map((el) => `<li>${el}</li>`).join("")}`;

  parent.insertAdjacentHTML("beforeend", DOMListAdd);
};

DOMArr(arrayDom1, document.body);
DOMArr(arrayDom2, document.body);
