jQuery(document).ready(function ($) {
  $(".header-item a").click(function () {
    let element = $(this).attr("href");
    if ($(element).length != 0) {
      $("html, body").animate(
        {
          scrollTop: $(element).offset().top - 100,
        },
        1000
      );
    }
    return false;
  });
});
$(function () {
  $(".up-button").click(function () {
    $("html, body").animate(
      {
        scrollTop: 0,
      },
      500
    );
  });
});
$(window).scroll(function () {
  if ($(this).scrollTop() > $(window).height()) {
    $(".up-button").fadeIn();
  } else {
    $(".up-button").fadeOut();
  }
});

$(function () {
  $("#slide").click(function () {
    $(".top-rated").slideToggle();
  });
});
