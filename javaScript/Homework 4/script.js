function createNewUser() {
  let firstName = prompt("Введіть ваше ім'я");
  let lastName = prompt("Введіть ваше прізвище");
  return (newUser = {
    firstname: firstName,
    lastname: lastName,
    getLogin: function () {
      let login = (this.firstname[0] + this.lastname).toLowerCase();
      return login;
    },
  });
}

console.log(createNewUser(), newUser.getLogin());

// Метод обьекта - это действие, которое дает возможность взаимодействовать с 
// обьектом и его свойствами (например метод - .this - означает, что обращение 
// происходит к конкретному свойству обьекта, в котором мы обращаемся)
