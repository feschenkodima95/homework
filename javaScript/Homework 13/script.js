// let link = document.getElementById("old-css");
const newCss = document.createElement("link");
const changeThemeHandler = () => {
  
  const head = document.getElementsByTagName("head")[0];

  newCss.type = "text/css";
  newCss.rel = "stylesheet";
  newCss.href = "style2.css";
  newCss.className = "new-css";
  // head.appendChild(newCss);

  let newCssId = document.getElementById("new-css");

  if (newCssId === true) {
    document.head.removeChild(newCssId);
  } else {
    head.appendChild(newCss);
  }
};



let changeButton = document.getElementById("theme-button");

changeButton.addEventListener("click", changeThemeHandler);

if (localStorage.getItem("style2.css") == 1) {
  newCss.href = "style2.css";
}

window.addEventListener("unload", () => {
  localStorage.setItem("style2.css", newCss.href.contains("style2.css") ? 1 : 0);
});

