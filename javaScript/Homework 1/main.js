let userAgeInput = +prompt('Type your age');
let userNameInput = prompt('Input your name');


if(userAgeInput >= 22){
    alert('Welcome, '+ userNameInput);
} else if(userAgeInput <= 22 && userAgeInput >= 18){
    let userAccepting = confirm('Are you sure you want to continue?');
    if(!userAccepting){
        alert('You are not allowed to visit this website.');
    } else
    alert('Welcome, '+ userNameInput);
} else {
    alert('You are not allowed to visit this website.')
};





// 1)
// var - переменные видны внутри функции и не видны за ее пределами, но может всплывать за пределы блока.
// let - переменные с блочной областью видимости, так же можно переназначить переменную. видна в пределах блока.
// const - переменные с блочной областью видимости, так же невозможно переназначить переменную. видна в пределах блока.

// 2)
// var не стоит использовать для избежания путаницы ненужного изменения кода.
// если переменную не нужно переназначать, то лучше использовать const, если же нужно переназначить в блоке, используем let.
