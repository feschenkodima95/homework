const buttonAccept = document.querySelector(".btn");

const passwordForm = document.querySelector(".password-form");

passwordForm.addEventListener("click", function (e) {
  let firstInputValue = document.querySelector(".password1").value;
  let secondInputValue = document.querySelector(".password2").value;
  let correction = document.querySelector(".correction");

  if (e.target.classList.contains("fa-eye")) {
    e.target.classList.replace("fa-eye", "fa-eye-slash");
    e.target.parentNode.childNodes[1].type = "password";
  } else if (e.target.classList.contains("fa-eye-slash")) {
    e.target.classList.replace("fa-eye-slash", "fa-eye");
    e.target.parentNode.childNodes[1].type = "text";
  }

  if (e.target.classList.contains("btn")) {
    e.preventDefault();
    if (firstInputValue === secondInputValue) {
      if (correction.style.display === "block") {
        correction.style.display = "none";
      }
      alert("You are welcome!");
    } else {
      correction.style.display = "block";
    }
  }
});
