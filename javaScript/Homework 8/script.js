const elContainer = document.querySelector(".price-container");

let inputObject = document.getElementById('price');

inputObject.onfocus = function(){
  inputObject.style.border = '3px solid green';
};

inputObject.onblur = function () {
  const elInput = document.getElementById("price");
  const value = elInput.value;

  if (value <= 0) {
    document.getElementById('correct-price').innerText =
      "Please, input correct price";
      elInput.style.border = '3px solid red';
      
  } else {
    document.getElementById('correct-price').innerText =
      "Price"; 
    const elNewItemList = document.createElement("span");
    elNewItemList.innerText = `Текущая цена: ${value}`;
    const elDeleteButton = document.createElement("button");
    elDeleteButton.innerText = "X";
    elDeleteButton.className = "js-remove-item";
    elNewItemList.append(elDeleteButton);
    elContainer.append(elNewItemList);
  }

 
};
const listHandler = (event) => {
  if (event.target.className.includes("js-remove-item")) {
    const parent = event.target.parentElement;
    parent.remove();
  }
};

elContainer.addEventListener("click", listHandler);
