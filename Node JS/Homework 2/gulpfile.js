const gulp = require("gulp");
const sass = require("gulp-sass");
const concat = require("gulp-concat");
const clean = require("gulp-clean");
const autoprefixer = require("gulp-autoprefixer");
const imagemin = require("gulp-imagemin");
const browsersync = require("browser-sync");
const uglify = require("gulp-uglify");
const jsMinify = require("gulp-js-minify");
const cleanCss = require("gulp-clean-css");
const browserSync = require("browser-sync");

gulp.task("HTML", () => {
  return gulp
    .src("src/*.html")
    .pipe(gulp.dest("build"))
    .pipe(browserSync.reload({ stream: true }));
});

gulp.task("CSS", () => {
  return gulp
    .src("src/scss/main.scss")
    .pipe(sass().on("error", sass.logError))
    .pipe(
      autoprefixer({
        cascade: false,
      })
    )
    .pipe(concat("styles.min.css"))

    .pipe(gulp.dest("build"))
    .pipe(browserSync.reload({ stream: true }));
});

gulp.task("JS", () => {
  return gulp
    .src("src/script/**/*.js")
    .pipe(concat("script.js"))
    .pipe(gulp.dest("build"))
    .pipe(browserSync.reload({ stream: true }));
});
gulp.task("imgmin", () => {
  return gulp
    .src("src/img/*")
    .pipe(imagemin([imagemin.mozjpeg({ quality: 75, progressive: true })]))
    .pipe(gulp.dest("build/img"));
});
gulp.task("clean", () => {
  return gulp.src("build", { read: false, allowEmpty: false }).pipe(clean());
});
gulp.task("browser", () => {
  browserSync.init({
    server: { baseDir: "./" },
  });
});

gulp.task("buildFile", (end) => {
  gulp.series("clean", gulp.parallel("HTML", "CSS", "JS", "imgmin"));
  end();
});

gulp.task("watch", () => {
  gulp
    .watch(
      "src/**/*.*",
      gulp.series("clean", gulp.parallel("HTML", "CSS", "JS", "imgmin"))
    )
    .on("change", browserSync.reload);
});
